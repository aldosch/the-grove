document.addEventListener('DOMContentLoaded', function() {
  // SMOOTH SCROLL ANCHOR LINKS
  // Finds link elements with href starting with #
  document.querySelectorAll('a[href^="#"]').forEach(anchor => {
    // Listens for click
    anchor.addEventListener('click', function(e) {
      // Prevents default behaviour
      e.preventDefault();

      // Gets value of the links href and scrolls into view using smooth behaviour
      document.querySelector(this.getAttribute('href')).scrollIntoView({
        behavior: 'smooth'
      });
    });
  });
});