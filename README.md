# The Grove
Townsquare technical test 12th of March 2019

**[thegrove.aldosch.com](https://thegrove.aldosch.com/)**

## Process
This is a brief explanation of how I tackled the project and what adventures I had along the way.

### Technical plan
I'm not very confident in my wordpress abilities so I decided to go for a [jamstack](https://jamstack.org/) approach which I'm more familiar with. 
* The site is built using [jekyll](https://jekyllrb.com) to allow simpler content changes and reusability of sections
* All content is stored here within gitlab to allow for full version control and for greater simplicity
* Its hosted on [netlify](http://netlify.com) which connects to the gitlab repo and dynamically updates the live site via a webhook when it detects a push to master
* I've configured netlify to run the jekyll build command and to fetch static content from the `_site` directory
* Netlify handles form submissions as this isn't one of my areas of strength and allows is a functional solution I can achieve within the limited timeframe 
* To increase development time I utilised bulma which is a css only front end framework that I'm familiar with. This meant I could create layout more efficiently
* For a CMS I could use either [forestry](https://forestry.io) or [cloudcannon](http://cloudcannon.com). 

### Development Process
These are the steps I went through to build the site

#### Started the tunes 🎵
First step of any project is to get some good music going. I chose an absolute banger of a [mixtape](https://youtu.be/d9vA7wFiiqE)

#### Initialise git
I set up a gitlab repo and cloned it locally. Created an ignore file and ensured I could push.

#### Set up jekyll
I set up a blank jekyll project. I ensured that it was configured correctly to work with [netlify](https://www.netlify.com/blog/2015/10/28/a-step-by-step-guide-jekyll-3.0-on-netlify/) by referencing this documentation as well as checking the official [jekyll docs](https://jekyllrb.com/docs/). After that I stripped all irrelevant content such as unneeded configs, directories and pages.

#### Extracted media assets
I imported the PDF into sketch and extracted SVGs for the accent lines, social icons, arrows and the map.

I converted the PSD photographs into JPGs. I then reduced the image sizes to more appropriate dimensions and more importantly file size. I then ran them through [ImageOptim](https://imageoptim.com/mac) to losslessly compress them further.

#### Fleshed out layout markup
My next step was to set up the basic layout for the site without styling using bulma's column system.

#### Ordered burrito 🌯
In the middle of this I ordered a burrito. This optimised my development performance ⚡️

#### Added some styling
Once the initial layout was set up I began adding styling and modifying the markup to work better with the intended designs.

I added some css transitions to button hover states and implemented the custom fonts.

#### Added JS for smooth anchor link transitions
I developed a simple script to scroll to the form nicely.

#### I called it a day here
This is where I got to on my first evening working on this. After working in the office from 7am to 11:30pm I was starting to make mistakes more often and called quits for the day. Nobody should work that long!

#### Day 2! Created custom button and form styles
Stripped all formatting from forms and added underlines. I also added a small hover animation to the submit button arrows.

#### Improved text styling and overall spacing
I created a few spacing classes to get the same effect as in the design. Also ensured that bulma's default text colour was being correctly overridden to black.

#### Periodically performed mobile testing
As I was working on this I checked mobile views to ensure that the spacing was ok on smaller screen sizes.

#### Added gradient to map section
I noticed that the text overlays onto the map and can cause readability/accessibility issues. Added a white gradient to the left hand side to remedy this.

#### Ordered ramen! 🍜
I smashed out some tasty ramen at this step.

#### Lots and lots of design improvements
This is probably my favourite bit because I can see the results really quickly and it feels like I'm making progress. 

#### Wrestled with the navbar for a while
Took me longer than I expected to figure out the correct flexbox work around to get the navbar to react the way I wanted to on mobile.

#### Wavy text!
This was the one I was really unsure about. I've never created something like this before. I did a lot of research to find the best way to achieve this and it looked like a vector based option was the best. I had initially tried to achieve it with css `transform` but that was taking way too long. 

I knew I needed to create an svg of the text in its curved state. I wasn't able to extract it from the PDF correctly because when I imported it into sketch the curve was there, but not the tilted angle and the font was also not carried across correctly. 

I then tried to create it myself using sketch's "text on path" feature. I needed a curved shape so I found a "sine wave" image online and pasted it in. I then traced it using the vector tool.

I used the new shape as my path and added text on top of it. This is where I hit trouble... I couldn't get the spacing right. Here's what my attempt looked like:

https://i.imgur.com/zKI3QTh.png

...very not ideal. 

As a result I decided not to include it in the template I created, but its noted here so you can see I tried! This is something I will have to learn and improve.

#### Go-live!
Final step was to push to netlify! Let's encrypt and all of that jazz was sorted pretty easily. I tested pushing to gitlab to see that it would build correctly and then configured the forms using netlify's api.

#### That's it!
done and dusted 😁

I was going to set up a CMS with cloudcannon but have run out of time this evening. Either way, the site is in a usable state!